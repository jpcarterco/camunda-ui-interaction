package orchestration.bpm;

import orchestration.entity.ZipCode;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.logging.Logger;

/***
 * This class uses Simple Java Mail Fluent API to send mail
 * http://www.simplejavamail.org/#/about
 * @author plungu
 *
 */
@Component("uiDelegateZipCodePublisher")
public class UiDelegateZipCodePublisher implements JavaDelegate {

    //@Autowired
    //ZipCodeSource source;

    public static Logger log = Logger.getLogger(UiDelegateZipCodePublisher.class.getName());

    public void execute(DelegateExecution execution) throws Exception {

        Map<String, Object> vars = execution.getVariables();
        log.info("[ ###### VARS ##### ]: "+vars);

        //If needed get the business key and place it into the message
        //Additionally get the object from Camunda and put it into the
        // message so the UI can consume it
        //String businessKey = execution.getBusinessKey();
        ZipCode zipCode = (ZipCode) execution.getVariable("zipCode");
        String zip = (String) execution.getVariable("zip");
        String bizKey = (String) execution.getBusinessKey();
        if (zipCode == null) {
            zipCode = new ZipCode();
            zipCode.setZip(zip);
            zipCode.setBusinessKey(bizKey);

        }
        zipCode.setBusinessKey(bizKey);
        log.info("zipcode is:" + zipCode.toString() + "");
        //zipCode.setBusinessKey(businessKey);

        //publish the message object to the message bus
        //source.publish().send(MessageBuilder.withPayload(zipCode).build());
        System.out.println("Contact Payload Sent: "+zipCode.toString());

    }

}