package orchestration.controller;

import orchestration.entity.ZipCode;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Map;

@Controller
public class NewZipCodeController {

    @Autowired
    RuntimeService runtimeService;

    @RequestMapping("/zipcode")
    public String zipCode(@RequestParam(value="zipCode", required=false, defaultValue="80210") String name, Model model) {
        model.addAttribute("zipCode", name);

        String bizKey = "messageTestKey";
        System.out.println("bizKey: " + bizKey);

        ZipCode zipCode = new ZipCode();
        zipCode.setZip(name);
        zipCode.setBusinessKey(bizKey);


        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("zipCode", zipCode);
        runtimeService.correlateMessage("messageEntry", bizKey, variables);
        //runtimeService.correlateMessage("zipCode-message", bizKey, variables);

        return "redirect:home";

    }

}
