package orchestration.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name="zipCode")
public class ZipCode implements Serializable {

    private static final long serialVersionUID = -209110232715280387L;

    public ZipCode(){}
    public ZipCode(String zip){
        this.zip = zip;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    public long getId() {
        return id;
    }

    @Column(nullable=true)
    private String zip;

    @Column(nullable=true)
    private String businessKey;

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}